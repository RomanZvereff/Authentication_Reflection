package com.zrv.authentication;

import com.sun.org.apache.xpath.internal.operations.String;

import javax.xml.bind.DatatypeConverter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Base64;

public class Reflection {

    final java.lang.String USERPASSWORD = "dGhpc2lzYXBhc3N3b3Jk";

    public void reflection(User user) {
        Class userClass = User.class;
        Field fields[] = userClass.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof Encrypted) {
                    field.setAccessible(true);
                    java.lang.String password = user.getAnyPassword();

                    field.set(user, new String(Base64.getEncoder().encode(password.getBytes())));
                }

            }


        }
    }  public boolean verification(User user){
        return user.getAnyPassword().equals(USERPASSWORD);
    }



//    public void encryptMethod() {
//        System.out.println(DatatypeConverter.printBase64Binary.getBytes());
//    }


}
//
